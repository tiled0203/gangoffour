package com.realdolmen.structural.flyweight;

import java.awt.*;
import java.util.HashMap;

//FLYWEIGHT PATTERN
// This factory only creates a new rectangle if it
// uses a color not previously used

// Intrinsic State: Color
// Extrinsic State: X & Y Values
public class RectFactory {
    // The HashMap uses the color as the key for every
    // rectangle it will make up to 8 total
    private static final HashMap<Color, MyRect> rectsByColor = new HashMap<Color, MyRect>();

    public static MyRect getRect(Color color) {
        MyRect rect = rectsByColor.get(color);
        if (rect == null) {
            rect = new MyRect(color);
            rectsByColor.put(color, rect);
        }
        return rect;
//        return null;
    }
}
