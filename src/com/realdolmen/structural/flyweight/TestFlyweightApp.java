package com.realdolmen.structural.flyweight;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import static java.awt.Color.*;
// The Flyweight design pattern is used when you need to
// create a large number of similar objects

// To reduce memory this pattern shares Objects that are
// the same rather than creating new ones
public class TestFlyweightApp extends JFrame {

    JButton startDrawing;

    int windowWidth = 1750;
    int windowHeight = 1000;
    private Color[] shapeColor = {ORANGE, BLUE, CYAN, RED, GRAY, GREEN, YELLOW, MAGENTA, BLACK};

    public static void main(String[] args) {
        new TestFlyweightApp();
    }

    public TestFlyweightApp() {
        // this is just to create the window
        this.setSize(windowWidth, windowHeight);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("FlyWeight test");
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout());
        final JPanel drawingPanel = new JPanel();

        startDrawing = new JButton("Draw Stuff");
        contentPane.add(drawingPanel, BorderLayout.CENTER);
        contentPane.add(startDrawing, BorderLayout.SOUTH);

        startDrawing.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Graphics g = drawingPanel.getGraphics();
                long startTime = System.currentTimeMillis(); //to see how long it will take to execute (start time)
                for (int i = 0; i < 100000; i++) { // this is to draw 100000 rectangles
//                    g.setColor(getRandColor());
//                    g.fillRect(getRandX(), getRandY(), getRandX(), getRandY());

//                    MyRect rect = new MyRect(getRandColor(),getRandX(),getRandY(),getRandX(),getRandY()); // slowest way
//                    rect.draw(g);

                    // Uses rectangles stored in the HashMap to
                    // speed up the program
                    MyRect rect = RectFactory.getRect(getRandColor());
                    rect.draw(g, getRandX(), getRandY(),getRandX(),getRandY());

                }
                long endTime = System.currentTimeMillis();//to see how long it will take to execute (end time)
                System.out.println("that took " + (endTime - startTime));
            }
        });

        this.add(contentPane);
        this.setVisible(true);
    }

    private Color getRandColor() {
        Random randomGenerator = new Random();
        int randInt = randomGenerator.nextInt(9);
        return shapeColor[randInt];
    }

    private int getRandX() {
        return (int) (Math.random() * windowWidth);
    }

    private int getRandY() {
        return (int) (Math.random() * windowHeight);
    }

}
