package com.realdolmen.structural.proxy;

/*
 *It can be used for security reasons, because an Object is intensive to create,
 * or is accessed from a remote location.
 * You can think of it as a gate keeper that blocks access to another Object.
 */
public class TestProxyApp {
    public static void main(String[] args) {
        ATMMachine atmMachine = new ATMMachine();
        atmMachine.insertCard();
        atmMachine.ejectCard();
        atmMachine.insertCard();
        atmMachine.insertPin(1234);
        atmMachine.requestCash(2000);
        atmMachine.insertCard();
//        atmMachine.insertPin(1234);

        GetATMData atmProxy = new ATMProxy();

        System.out.println("Current ATM state " + atmProxy.getATMState());
        System.out.println("Cash in ATM mashine " + atmProxy.getCashInMachine());

        GetATMData realATMMachine = new ATMMachine();


//        realATMMachine.setCashInMachine(400); // not accessible
//        atmProxy.setCashInMachine(400); // not accessible


    }
}
