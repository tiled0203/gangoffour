package com.realdolmen.structural.proxy.proxy;

import java.util.HashMap;

public class MapProxy implements AbstractMap {

	private String fileName;
	private HashMap<String, String> hashtable = new HashMap<String, String>();
	private final Map map;

	public MapProxy(String fileName) {
		map = new Map(fileName);
	}

	public String find(String key) throws Exception {
		String foundValue = get(key);
		if(foundValue != null){
			return foundValue;
		}else {
			return map.find(key);
		}
	}

	public void add(String key, String value) throws Exception {
		String foundValue = hashtable.get(key);
		if(foundValue == null){
			put(key,value);
			map.add(key, value);
		}
	}

	private String get(String key) {
		return (String) hashtable.get(key);
	}

	private void put(String key, String value) {
		hashtable.put(key, value);
	}
}
