package com.realdolmen.structural.bridge;

public class TestTheRemoteApp {
    public static void main(String[] args) {
        RemoteButton theTv = new TVRemoteMute(new TVDevice(1, 200));
        RemoteButton theTv2 = new TVRemotePause(new TVDevice(1, 200));


        System.out.println("Test TV with mute");
        theTv.buttonFivePressed();
        theTv.buttonSixPressed();
        theTv.buttonNinePressed();

        System.out.println("Test TV with pause");
        theTv2.buttonFivePressed();
        theTv2.buttonSixPressed();
        theTv2.buttonSixPressed();
        theTv2.buttonSixPressed();
        theTv2.buttonSixPressed();
        theTv2.buttonNinePressed();
        theTv2.deviceFeedback();
    }
}
