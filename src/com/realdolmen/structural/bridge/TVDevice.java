package com.realdolmen.structural.bridge;

public class TVDevice extends EntertainmentDevice {

    public TVDevice(int newDeviceState, int newMaxSetting) {
        this.deviceState = newDeviceState;
        this.maxSetting = newMaxSetting;
    }

    @Override
    public void buttonFivePressed() {
        System.out.println("Channel down");
        deviceState--;
    }

    @Override
    public void buttonSixPressed() {
        System.out.println("Channel up");
        deviceState++;
    }
}
