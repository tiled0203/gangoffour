#When to use the bridge pattern?

- When you want to be able to change both the abstractions (abstract classes ) and concrete classes independently
- When you want the first abstract class to define rules (Abstract TV)
- The concrete class adds additional rules (Concrete TV)
- An abstract class has a reference to the device, and it defines abstract methods
 that will be defined (Abstract Remote)
 - The concrete Remote defines the abstract methods required

![picture](Bridge_example.jpg)
---
![picture](Bridge_pattern_participants.png)
