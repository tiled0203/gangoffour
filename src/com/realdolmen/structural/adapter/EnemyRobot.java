package com.realdolmen.structural.adapter;

import java.util.Random;

// This is the Adaptee. The Adapter sends method calls
// to objects that use the EnemyAttacker interface
// to the right methods defined in EnemyRobot
public class EnemyRobot {
    Random generator = new Random();

    public void smashWithHands() {
        int attackDamage = generator.nextInt(10) + 1;
        System.out.println("Enemy robot causes " + attackDamage + " Damage with its hands");
    }

    public void walkForward(){
        int movement = generator.nextInt(5) + 1;
        System.out.println("Enemy robot walks forward " + movement + " spaces");
    }

    public void reactToHuman(String driverName){
        System.out.println("Enemy robot tramps on " + driverName);
    }
}
