package com.realdolmen.structural.adapter;

public class TestEnemyAttackersApp {
    public static void main(String[] args) {
        EnemyRobot fredRobot = new EnemyRobot();
        EnemyTank rx7Tank = new EnemyTank();
        EnemyAttacker robotAdapter = new EnemyRobotAdapter(fredRobot);

        System.out.println("The AI robot");
        fredRobot.reactToHuman("Chris");
        fredRobot.walkForward();
        fredRobot.smashWithHands();

        System.out.println("The enemy tank");
        rx7Tank.assignDriver("Frank");
        rx7Tank.moveForward();
        rx7Tank.fireWeapon();

        System.out.println("The robot with adapter");
        robotAdapter.assignDriver("Mark");
        robotAdapter.moveForward();
        robotAdapter.fireWeapon();
    }
}
