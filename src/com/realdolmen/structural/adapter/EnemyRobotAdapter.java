package com.realdolmen.structural.adapter;

// The Adapter must provide an alternative action for
// the the methods that need to be used because
// EnemyAttacker was implemented.

// This adapter does this by containing an object
// of the same type as the Adaptee (EnemyRobot)
// All calls to EnemyAttacker methods are sent
// instead to methods used by EnemyRobot
public class EnemyRobotAdapter implements EnemyAttacker {
    EnemyRobot enemyRobot; //adaptee

    public EnemyRobotAdapter(EnemyRobot newRobot) {
        this.enemyRobot = newRobot;
    }

    @Override
    public void fireWeapon() {
        enemyRobot.smashWithHands();
    }

    @Override
    public void moveForward() {
        enemyRobot.walkForward();
    }

    @Override
    public void assignDriver(String driverName) {
        enemyRobot.reactToHuman(driverName);
    }
}
