package com.realdolmen.structural.facade;

public class TestFacadeApp {
    public static void main(String[] args) {
        BankAccountFacade accessingBank = new BankAccountFacade(12345678,1234);
        accessingBank.withdrawCash(50.00);
        accessingBank.withdrawCash(800.00);
        accessingBank.depositCash(100.00);
    }
}
