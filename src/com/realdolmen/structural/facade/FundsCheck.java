package com.realdolmen.structural.facade;

public class FundsCheck {
    private double chashInAccount = 10000.0;

    public double getChashInAccount() {
        return chashInAccount;
    }

    public void decreaseCashInAccount(double cashWithdrawn) {
        chashInAccount -= cashWithdrawn;
    }

    public void increaseCashInAccount(double cashDeposited) {
        chashInAccount += cashDeposited;
    }

    public boolean haveEnoughMoney(double cashToWithdrawal) {
        if (cashToWithdrawal > getChashInAccount()) {
            System.err.println("Error: you don't have enough money");
            System.out.println("Current balance: " + getChashInAccount());
            return false;
        } else {
            decreaseCashInAccount(cashToWithdrawal);
            System.out.println("Withdrawal complete: current balance " + getChashInAccount());
            return true;
        }
    }

    public void makeDeposit(double cashToDeposit){
        increaseCashInAccount(cashToDeposit);
        System.out.println("Deposit complete: current balance " + getChashInAccount());
    }
}
