package com.realdolmen.structural.facade;

// The Facade Design Pattern decouples or separates the client
// from all of the sub components

// The Facades aim is to simplify interfaces so you don't have
// to worry about what is going on under the hood
public class BankAccountFacade {
    private int accountNumber;
    private int securityCode;

    private AccountNumberCheck accountNumberCheck;
    private SecurityCodeCheck securityCodeCheck;
    private FundsCheck fundsCheck;
    private WelcomeToBank welcomeToBank;

    public BankAccountFacade(int newAcctNum, int newSecCode) {
        this.accountNumber = newAcctNum;
        this.securityCode = newSecCode;
        welcomeToBank = new WelcomeToBank();
        accountNumberCheck = new AccountNumberCheck();
        securityCodeCheck = new SecurityCodeCheck();
        fundsCheck = new FundsCheck();
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getSecurityCode() {
        return securityCode;
    }

    public void withdrawCash(double cashToGet) {
        if (accountNumberCheck.accountActive(getAccountNumber()) &&
                securityCodeCheck.isCodeCorrect(getSecurityCode()) &&
                fundsCheck.haveEnoughMoney(cashToGet)) {
            System.out.println("Transaction complete");
        } else {
            System.out.println("Transaction failed");
        }
    }

    public void depositCash(double cashToDeposit) {
        if (accountNumberCheck.accountActive(getAccountNumber()) &&
                securityCodeCheck.isCodeCorrect(getSecurityCode())) {
            fundsCheck.makeDeposit(cashToDeposit);
            System.out.println("Transaction complete");
        } else {
            System.out.println("Transaction failed");
        }
    }
}
