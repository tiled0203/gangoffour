package com.realdolmen.structural.composite;

import java.util.ArrayList;
import java.util.Iterator;

public class SongGroup extends SongComponent {
    // Contains any Songs or SongGroups that are added
    // to this ArrayList
    ArrayList songComponents = new ArrayList();
    String groupName;
    String groupDescription;

    public SongGroup(String newGroupName, String newGroupDescription) {
        this.groupName = newGroupName;
        this.groupDescription = newGroupDescription;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void add(SongComponent newSongComponent) {
        songComponents.add(newSongComponent);
    }


    public void remove(SongComponent songComponent){
        songComponents.remove(songComponent);
    }

    public SongComponent getSongComponent(int componentIndex) {
        return (SongComponent) songComponents.get(componentIndex);
    }

    public void displaySongInfo(){
        System.out.println(getGroupName() + " " + getGroupDescription());
        Iterator songIterator = songComponents.iterator(); //iterator pattern
        // Cycles through and prints any Songs or SongGroups added
        // to this SongGroups ArrayList songComponents
        while (songIterator.hasNext()){
            SongComponent songInfo = (SongComponent) songIterator.next();
            songInfo.displaySongInfo();
        }
    }

}
