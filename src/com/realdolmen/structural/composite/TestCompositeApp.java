package com.realdolmen.structural.composite;

public class TestCompositeApp {
    public static void main(String[] args) {
        SongComponent industrialMusic = new SongGroup("Industrial", "Industrial music is a genre of music that draws on harsh, transgressive or provocative sounds and themes");
        SongComponent heavyMetalMusic = new SongGroup("Heavy Metal Music", "heavy metal bands developed a thick, massive sound, characterized by distortion, extended guitar solos, emphatic beats, and loudness");
        SongComponent hardRockMusic = new SongGroup("Rock Music", "Rock music is a broad genre of popular music that originated as \"rock and roll\"");

        // Top level component that holds everything
        SongComponent everySong = new SongGroup("Song list", "Every song available");
        // Composite that holds individual groups of songs
        // This holds Songs plus a SongGroup with Songs
        everySong.add(industrialMusic);
        industrialMusic.add(new Song("Head like a hole", "NIN", 1990));
        industrialMusic.add(new Song("Headhunter", "Front 242", 1988));
        industrialMusic.add(heavyMetalMusic);

        everySong.add(heavyMetalMusic);
        // This is a SongGroup that just holds Songs
        heavyMetalMusic.add(new Song("Your betrayal", "Bullet for My Valentine", 2010));
        heavyMetalMusic.add(new Song("The Last stand", "Sabaton", 2016));

        everySong.add(hardRockMusic);
        // This is a SongGroup that just holds Songs
        hardRockMusic.add(new Song("Hail to the king", "Avenged Sevenfold", 2013));
        hardRockMusic.add(new Song("Wind of Change", "Scorpions", 1990));

        DiscJockey blackWolf = new DiscJockey(everySong);
        blackWolf.getSongList();
    }
}
