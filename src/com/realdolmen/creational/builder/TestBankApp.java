package com.realdolmen.creational.builder;

public class TestBankApp {
    public static void main(String[] args) {
        //otherwise it would look like this
        //BankAccount account = new BankAccount(456L, "Marge", "Springfield", 100.00, 2.5);
        //BankAccount anotherAccount = new BankAccount(789L, "Homer", null, 2.5, 100.00);  //Oops!
        BankAccount account = new BankAccount.Builder(1234L)
                .withOwner("David")
                .atBranch("Springfield")
                .openingBalance(100)
                .atRate(2.5)
                .build();
        BankAccount anotherAccount = new BankAccount.Builder(4567L)
                .withOwner("Johnny")
                .atBranch("Springfield")
                .openingBalance(100000000)
                .atRate(2.5)
                .build();

        System.out.printf("Owner: %s AccountNumber: %d OpeningBalance: %f \n"
                , account.getOwner()
                , account.getAccountNumber()
                , account.getBalance());
        System.out.printf("Owner: %s InterestRate: %f Branch: %s"
                , anotherAccount.getOwner()
                , anotherAccount.getInterestRate()
                , anotherAccount.getBranch());

    }
}
