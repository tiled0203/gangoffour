package com.realdolmen.creational.prototype;

public class TestPrototypeApp {
    public static void main(String[] args) {
        CloneLab animalMaker = new CloneLab();
        Sheep sally = new Sheep();
        Sheep clonedSheep = (Sheep) animalMaker.getClone(sally);
        System.out.println(sally);
        System.out.println(clonedSheep);
        System.out.println("Sally hashcode: " + System.identityHashCode(System.identityHashCode(sally))); // to prove that it's a different object
        System.out.println("Clone hashcode: " + System.identityHashCode(System.identityHashCode(clonedSheep)));
    }
}
