package com.realdolmen.creational.prototype;

//the PrototypeFactory
public class CloneLab {
    public Animal getClone(Animal animalSample) {
        return animalSample.makeCopy();
    }
}
