package com.realdolmen.creational.prototype;


public interface Animal extends Cloneable {
    public Animal makeCopy();
}
