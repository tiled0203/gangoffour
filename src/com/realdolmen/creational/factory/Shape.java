package com.realdolmen.creational.factory;

public interface Shape {
    void draw();
}
