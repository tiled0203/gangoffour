package com.realdolmen.behavioural.iterator;

public class Topic {

    private String message;

    public Topic(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
