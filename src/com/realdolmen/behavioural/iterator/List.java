package com.realdolmen.behavioural.iterator;

public interface List<E> {

    Iterator<E> iterator();
}
