package com.realdolmen.behavioural.mediator;

public class TestChatRoom {

    public static void main(String[] args) {
        IChatRoom chatroom = new ChatRoom();
        User user1 = new ChatUser(chatroom, "1", "Alex");
        User user2 = new ChatUser(chatroom, "2", "Brian");
        User user3 = new ChatUser(chatroom, "3", "Charles");
        User user4 = new ChatUser(chatroom, "4", "David");

        chatroom.addUser(user1);
        chatroom.addUser(user2);
        chatroom.addUser(user3);
        chatroom.addUser(user4);


        user1.send("Hello Brian","2"); //Alex sends to Brian
        user2.send("Hey buddy","1");//Brian sends to Alex
    }
}
