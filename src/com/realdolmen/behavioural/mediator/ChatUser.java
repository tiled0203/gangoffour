package com.realdolmen.behavioural.mediator;

public class ChatUser extends User {

    public ChatUser(IChatRoom mediator, String id, String name) {
        super(mediator, id, name);
    }

    @Override
    public void send(String msg, String userId) {
        System.out.printf("%s sending message : %s\n",this.getName(),msg);
        getMediator().sendMessage(msg,userId);
    }

    @Override
    public void receive(String msg) {
        System.out.printf("%s received message : %s\n",this.getName(),msg);
    }
}
