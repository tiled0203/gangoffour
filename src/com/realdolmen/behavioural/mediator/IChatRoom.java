package com.realdolmen.behavioural.mediator;

public interface IChatRoom {

    //Mediator, Interface for communication between user objects

    public void sendMessage(String msg, String userId);

    public void addUser(User user);
}
