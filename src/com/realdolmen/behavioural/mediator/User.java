package com.realdolmen.behavioural.mediator;

public abstract class User {
    private IChatRoom mediator;
    private String name;
    private String id;

    public User(IChatRoom mediator, String id, String name) {
        this.mediator = mediator;
        this.name = name;
        this.id = id;
    }

    public abstract void send(String msg, String userId);
    public abstract void receive(String msg);

    public IChatRoom getMediator() {
        return mediator;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }
}
