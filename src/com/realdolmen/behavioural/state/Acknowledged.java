package com.realdolmen.behavioural.state;

public class Acknowledged implements PackageState{

    private static Acknowledged instance;

    private Acknowledged() {
    }

    public static Acknowledged getInstance(){
        if(instance==null){
            instance = new Acknowledged();
        }
        return instance;
    }

    @Override
    public void updateState(DeliveryContext deliveryContext) {
        System.out.println("Package is acknowledged !!");
        deliveryContext.setCurrentState(Shipped.getInstance());

    }
}
