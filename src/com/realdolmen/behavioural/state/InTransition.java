package com.realdolmen.behavioural.state;

public class InTransition implements PackageState {

    private static InTransition instance;

    private InTransition() {
    }

    public static InTransition getInstance(){
        if(instance == null){
            instance = new InTransition();
        }
        return instance;
    }

    @Override
    public void updateState(DeliveryContext deliveryContext) {

        System.out.println("Package is in transition !!");
        deliveryContext.setCurrentState(OutForDelivery.getInstance());
    }
}
