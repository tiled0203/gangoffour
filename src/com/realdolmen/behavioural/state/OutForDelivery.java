package com.realdolmen.behavioural.state;

public class OutForDelivery implements PackageState{

    private static OutForDelivery instance;

    private OutForDelivery(){}

    public static OutForDelivery getInstance(){
        if(instance==null){
            instance = new OutForDelivery();
        }
        return instance;
    }

    @Override
    public void updateState(DeliveryContext deliveryContext) {
        System.out.println("Package is out for delivery !!");
        deliveryContext.setCurrentState(Delivered.getInstance());
    }
}
