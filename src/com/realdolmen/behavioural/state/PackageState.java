package com.realdolmen.behavioural.state;

public interface PackageState {

    void updateState(DeliveryContext deliveryContext);
}
