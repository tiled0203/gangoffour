package com.realdolmen.behavioural.state;

public class Delivered implements PackageState {

    private static Delivered instance;

    private Delivered(){}

    public static Delivered getInstance() {
        if(instance == null){
            instance = new Delivered();
        }
        return instance;
    }

    @Override
    public void updateState(DeliveryContext deliveryContext) {
        System.out.println("Package is delivered!!");
    }
}
