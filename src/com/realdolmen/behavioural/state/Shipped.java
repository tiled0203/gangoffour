package com.realdolmen.behavioural.state;

public class Shipped implements PackageState{

    private static Shipped instance;

    private Shipped() {}

    public static Shipped getInstance() {
        if(instance == null){
            instance = new Shipped();
        }
        return instance;
    }

    //Business logic and state transition
    @Override
    public void updateState(DeliveryContext ctx)
    {
        System.out.println("Package is shipped !!");
        ctx.setCurrentState(InTransition.getInstance());
    }

}
