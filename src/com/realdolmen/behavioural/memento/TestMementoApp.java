package com.realdolmen.behavioural.memento;

public class TestMementoApp {
    //this class is the caretaker for the article memento

    public static void main(String[] args) {
        Article article = new Article(1,"My article","content of this article");
        Memento memento = article.createMemento(); //immutable memento

        System.out.println(article);

        article.setContent("new content of this article");
        article.setTitle("new title for this article");

        System.out.println(article);

        article.restore(memento);

        System.out.println(article);
    }


}
