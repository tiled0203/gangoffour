package com.realdolmen.behavioural.memento;

public interface IMementoOriginator<E> {
    //work to an interface
    //this is not in the documentation, but it should be

    E createMemento();
    void restore(E e);

}
