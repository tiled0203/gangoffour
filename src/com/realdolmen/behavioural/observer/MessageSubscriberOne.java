package com.realdolmen.behavioural.observer;

public class MessageSubscriberOne implements Observer {
    @Override
    public void update(Message m) {
        System.out.printf("Message subscriber one : %s\n",m.getMessageContent());
    }
}
