package com.realdolmen.behavioural.observer;

import java.util.ArrayList;
import java.util.List;

public class MessagePublisher implements Subject{

    //Subject implementation, list with observers
    List<Observer> observers = new ArrayList<>();

    @Override
    public void attach(Observer o) {
        observers.add(o);
    }

    @Override
    public void detach(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyUpdate(Message m) {
//        observers.forEach(o -> o.update(m)); //lambda
        for (Observer observer : observers){
           observer.update(m);
        }
    }
}
