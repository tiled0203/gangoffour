package com.realdolmen.behavioural.observer;

public class Message {

    //immutable, so no class can modify by mistake
    private final String messageContent;

    public Message(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getMessageContent() {
        return messageContent;
    }
}
