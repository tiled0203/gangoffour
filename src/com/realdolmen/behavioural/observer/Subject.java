package com.realdolmen.behavioural.observer;


public interface Subject {

    //the subject to observe


    void attach(Observer o);
    void detach(Observer o);
    void notifyUpdate(Message m);
}
