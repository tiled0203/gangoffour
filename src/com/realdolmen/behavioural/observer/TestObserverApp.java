package com.realdolmen.behavioural.observer;

public class TestObserverApp {
    public static void main(String[] args) {
        Observer o1 = new MessageSubscriberOne();
        Observer o2 = new MessageSubscriberTwo();

        MessagePublisher publisher = new MessagePublisher();
        publisher.attach(o1);

        publisher.notifyUpdate(new Message("First Message"));   //o1 will receive the update

        publisher.attach(o2);

        publisher.notifyUpdate(new Message("Second Message")); //o1 and o2 will receive the update

        publisher.attach(o1);

        publisher.notifyUpdate(new Message("Third Message")); //o2 will receive the update

        publisher.detach(o1);

        publisher.notifyUpdate(new Message("Fourth Message")); //o2 will receive the update

    }

}
