package com.realdolmen.behavioural.observer.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {

    //Subject implementation, list with observers
    private List<Observer> observers = new ArrayList<>();

    public final void attach(Observer o) {
        observers.add(o);
    }


    public final void detach(Observer o) {
        observers.remove(o);
    }


    public final void notifyUpdate() {
//        observers.forEach(o -> o.update(m)); //lambda
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
