package com.realdolmen.behavioural.observer.observer;




public class Person extends Subject {

    private String forename;
    private String surname;
    private int age;

    public Person() {
        forename = "NONE";
        surname = "NONE";
        age = 0;
    }

    public Person(String f, String s, int a) {
        forename = f;
        surname = s;
        age = a;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public void setForename(String f) {
        forename = f;
        super.notifyUpdate();
    }

    public void setSurname(String s) {
        surname = s;
        super.notifyUpdate();
    }

    public void setAge(int a) {
        age = a;
        super.notifyUpdate();
    }

    public void increaseAge(int n) {
        age += n;
        super.notifyUpdate();
    }

}
