package com.realdolmen.behavioural.observer.observer;


public interface Observer {

    void update();
}
