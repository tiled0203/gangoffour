package com.realdolmen.behavioural.observer;


public interface Observer {

    //will observe a subject
    void update(Message m);
}
