package com.realdolmen.behavioural.observer;

public class MessageSubscriberTwo implements Observer {
    @Override
    public void update(Message m) {
        System.out.printf("Message subscriber two : %s\n", m.getMessageContent());
    }
}
