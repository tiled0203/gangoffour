package com.realdolmen.behavioural.command;

public interface Command {
    void execute();
}
