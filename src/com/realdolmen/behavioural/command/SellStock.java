package com.realdolmen.behavioural.command;

//the command
public class SellStock implements Command {
    private Stock abcStock = new Stock(); // the receiver

    public SellStock(){
    }

    public void execute() {
        abcStock.sell();
    }
}
