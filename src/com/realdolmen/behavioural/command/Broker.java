package com.realdolmen.behavioural.command;

import java.util.ArrayList;
import java.util.List;

//the invoker
public class Broker {
    private List<Command> commandList = new ArrayList<Command>();

    public void takeOrder(Command command) {
        commandList.add(command);
    }

    public void placeOrders() {
        for (Command command : commandList) {
            command.execute();
        }
        commandList.clear();
    }
}

