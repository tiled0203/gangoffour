package com.realdolmen.behavioural.command;

//the command
public class BuyStock implements Command {
    private Stock abcStock = new Stock(); // the receiver

    public BuyStock(){
    }

    public void execute() {
        abcStock.buy();
    }
}
