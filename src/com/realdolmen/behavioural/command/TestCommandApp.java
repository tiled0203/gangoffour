package com.realdolmen.behavioural.command;

//the manager that gives commands to the broker
public class TestCommandApp {
    public static void main(String[] args) {
        //the commands
        BuyStock buyStockOrder = new BuyStock();
        SellStock sellStockOrder = new SellStock();

        //the invoker
        Broker broker = new Broker();
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(sellStockOrder);

        broker.placeOrders();
    }
}
